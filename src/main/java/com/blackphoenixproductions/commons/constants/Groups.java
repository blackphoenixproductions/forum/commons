package com.blackphoenixproductions.commons.constants;

public class Groups {
    public static final String NOTIFICATIONS_CONSUMER = "notifications-consumer";
    public static final String POSTS_CONSUMER = "posts-consumer";
    public static final String TOPICS_CONSUMER = "topics-consumer";

    private Groups() {
    }
}
