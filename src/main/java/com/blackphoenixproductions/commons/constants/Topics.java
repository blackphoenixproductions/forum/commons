package com.blackphoenixproductions.commons.constants;

public class Topics {
    public static final String REFRESH_MV_TOPICS = "refresh-mv-topics";
    public static final String REFRESH_MV_POSTS = "refresh-mv-posts";
    public static final String NOTIFICATIONS = "notifications";

    private Topics() {
    }
}
