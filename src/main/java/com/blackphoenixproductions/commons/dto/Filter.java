package com.blackphoenixproductions.commons.dto;

import com.blackphoenixproductions.commons.enums.BooleanOperator;
import com.blackphoenixproductions.commons.enums.QueryOperator;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
public class Filter {
    BooleanOperator booleanOperator;
    QueryOperator queryOperator;
    String field;
    String value;
    List<String> values; // in caso di operatore IN e BETWEEN
    @Builder.Default List<Filter> filters = new ArrayList<>();
}
