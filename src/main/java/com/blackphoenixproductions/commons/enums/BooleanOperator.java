package com.blackphoenixproductions.commons.enums;

public enum BooleanOperator {
    AND,
    OR
}
