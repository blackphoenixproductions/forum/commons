package com.blackphoenixproductions.commons.enums;

public enum Claims {
    EMAIL ("email"),
    USERNAME ("preferred_username"),
    REALM ("realm_access"),
    ROLES ("roles");

    private final String value;

    Claims(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
