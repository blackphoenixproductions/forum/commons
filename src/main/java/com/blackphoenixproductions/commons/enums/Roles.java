package com.blackphoenixproductions.commons.enums;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum Roles {

    ROLE_USER ("USER"),
    ROLE_STAFF ("STAFF"),
    ROLE_HELPDESK ("HELPDESK");

    private final String value;

    Roles(String value) {
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static final List<String> getAllRoles(){
        List<Enum> enumValues = Arrays.asList(Roles.values());
        return enumValues.stream().map(e -> e.toString()).collect(Collectors.toList());
    }


}
